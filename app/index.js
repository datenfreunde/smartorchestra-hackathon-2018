// =================================================================================
// App Configuration
// =================================================================================

const {App} = require('jovo-framework');
const ApiService = require('./ApiService');
const SETTINGS = require('./settings');
const LogUtil = require('./utils/LogUtil');

let myRequestLoggingObjects = ['request'];
let myResponseLoggingObjects = ['response'];

const config = {
    requestLoggingObjects: myRequestLoggingObjects,
    responseLoggingObjects: myResponseLoggingObjects,
    logging: true,
};

const app = new App(config);
const api = new ApiService();
// =================================================================================
// App Logic
// =================================================================================

app.setHandler({
    'LAUNCH': function() {
        LogUtil.log('LAUNCH');
        this.toIntent('StartIntent');
    },

    'StartIntent': function() {
        let bodyTemplate2 = this.alexaSkill().templateBuilder('BodyTemplate2');
        bodyTemplate2.setToken('token').setTitle('Smart Orchestra').setTextContent('', '', '').setRightImage({
            description: 'Smart Orchestra Logo',
            url: 'http://smartorchestra.de/wp-content/uploads/2015/07/Bildmarke_smartOrchestra.png',
        });

        this.alexaSkill().showDisplayTemplate(bodyTemplate2);
        /*this.ask(
            'Hallo. Willkommen beim Smart Orchestra. Derzeit steht ein Statuswert für Wasser zur Verfügung. Möchtest Du diesen Wert wissen?');*/
        //this.ask('Wasser oder Schimmelrisiko?');
        let speech = 'Hallo. Herzlich Willkommen beim Smart Orchestra Skill der Datenfreunde. ';
        speech += 'Derzeit steht ein Wasserverbrauch der Regio IT und eine Information zum Schimmelrisiko von Cleopa zur Verfügung, ';
        speech += 'Was möchtest Du abfragen? Sage dazu Wasserverbrauch oder Schimmelrisiko';
        this.ask(speech);
    },

    'WaterIntent': function() {
        LogUtil.log('------------- ');
        LogUtil.log('WaterIntent');
        LogUtil.log('------------- ');
        //this.tell('Im Gebäude Hauptstraße beträgt der Zählerstand heute 534.899 kubikmeter. Der Verbrauch in den letzten 12 Stunden ist normal.');
        api.getDataDirect().then((data) => {
            LogUtil.log(data);
            if (data) {
                let bodyTemplate2 = this.alexaSkill().templateBuilder('BodyTemplate2');
                bodyTemplate2.setToken('token').
                    setTitle('Smart Orchestra').
                    setTextContent('Wasser Verbrauch', '', '').
                    setRightImage({
                        description: 'Smart Orchestra Logo',
                        url: 'http://smartorchestra.de/wp-content/uploads/2015/07/Bildmarke_smartOrchestra.png',
                    });
                this.alexaSkill().showDisplayTemplate(bodyTemplate2);
                this.tell(data.ssml);
            }
        });

        /*api.getData()
        .then((fetchedData) => {
            LogUtil.log('result data', fetchedData);

            if (fetchedData) {

                let set = fetchedData.filter((d) => {
                    return d.id === 'e2watch-report'
                });
                set = set[0];
                let data = set.report.value;
                LogUtil.log(set);



                let counter = 'Aktueller Zählerstand: ' + data.water + ' m3';
                let delta = 'Vergleich: ' + data.delta + ' m3';

                let bodyTemplate2 = this.alexaSkill().templateBuilder('BodyTemplate2');
                bodyTemplate2
                .setToken('token')
                .setTitle('Smart Orchestra')
                .setTextContent('Wasser Status', counter, delta)
                .setRightImage({
                    description: 'Smart Orchestra Logo',
                    url: 'http://smartorchestra.de/wp-content/uploads/2015/07/Bildmarke_smartOrchestra.png',
                });

                this.alexaSkill().showDisplayTemplate(bodyTemplate2);
                let speech = "Der aktuelle Wasserverbrauch beträgt: " + data.water + " kubikmeter.";
                speech += " Der Verbrauch erhöhte sich im Vergleich um: " + data.delta + ' kubikmeter';
                this.tell(speech);
            }
        })
        .catch((error) => {
            LogUtil.error(error);
            this.tell('Es ist leider ein Fehler beim Abruf der Daten aufgetreten. Bitte versuche es später noch einmal.')
        })*/
    },

    'MoldIntent': function() {
        LogUtil.log('------------- ');
        LogUtil.log('MoldIntent');
        LogUtil.log('------------- ');
        let bodyTemplate2 = this.alexaSkill().templateBuilder('BodyTemplate2');
        bodyTemplate2.setToken('token').
            setTitle('Smart Orchestra').
            setTextContent('Luftfeuchtigkeit: 43%', 'Temperatur: 21,7 Grad Celsius', 'Schimmelrisiko: gering').
            setRightImage({
                description: 'Smart Orchestra Logo',
                url: 'http://smartorchestra.de/wp-content/uploads/2015/07/Bildmarke_smartOrchestra.png',
            });

        this.alexaSkill().showDisplayTemplate(bodyTemplate2);
        this.tell(
            'Deine aktuelle Luftfeuchtigkeit im Gebäude Lagerhausstraße beträgt 43 Prozent und die Temperatur beträgt 21,7 Grad Celsius. Das Schimmelrisiko ist gering.');
    },

    'AMAZON.YesIntent': function() {
        LogUtil.log('------------- ');
        LogUtil.log('AMAZON.YesIntent');
        api.getData().then((data) => {
            if (data) {
                let set = data.filter((d) => {
                    return d.id === 'hackathonTemp';
                });
                set = set[0];
                LogUtil.log(set);

                let bodyTemplate2 = this.alexaSkill().templateBuilder('BodyTemplate2');
                bodyTemplate2.setToken('token').
                    setTitle('Smart Orchestra').
                    setTextContent('Aktueller Wert', set.temp1.value.toString(), '').
                    setRightImage({
                        description: 'Smart Orchestra Logo',
                        url: 'http://smartorchestra.de/wp-content/uploads/2015/07/Bildmarke_smartOrchestra.png',
                    });

                this.alexaSkill().showDisplayTemplate(bodyTemplate2);

                this.tell('Der aktuelle Wert ist: ' + set.temp1.value);
            }
        }).catch((error) => {
            LogUtil.error(error);
            this.tell(
                'Es ist leider ein Fehler beim Abruf der Daten aufgetreten. Bitte versuche es später noch einmal.');
        });
    },

    'AMAZON.NoIntent': function() {
        this.tell('okay');
    },

    'AMAZON.HelpIntent': function() {
        /*this.tell('Derzeit steht ein Statuswert für Wasser zur Verfügung.')
            .reprompt('Möchtest Du diesen Wert wissen?')*/
    },

    /*'StartWithParameter': function() {
        LogUtil.log("StartWithParameter has menu_item?", this.alexaSkill().hasSlotValue('menu_item'));


        if(this.alexaSkill().hasSlotValue('menu_item')) {
            let action = this.getInput('menu_item').value;
            LogUtil.log('MENU ITEM VALUE: ', action);
            switch(action.toLowerCase()) {
                case 'wasser':
                    this.tell('Hier kommen die Statusdaten');
                    break;
                case 'strom':
                    this.tell('Hier kommt der Vergleich');
                    break;
                case 'gas':
                    this.tell('Hier kommt die Vorhersage');
                    break;
                default:
                    this.alexaSkill().dialogElicitSlot('menu_item', 'Ich habe dich nicht richtig verstanden. Bitte sage Wasser, Strom oder Gas', 'Mögliche Antworten: Wasser, Strom, Gas ');
            }
        }
        else {
            this.alexaSkill().dialogDelegate();
        }
    },

    'GetDataIntent': function () {
        api.getData()
            .then(data => {
                if(data) {
                    this.tell(data);
                }
                else {
                    this.tell("Leider konnte ich keine Daten laden!");
                }

            });
    },*/
});

exports.handler = (event, context, callback) => {
    console.log('init handler');
    app.handleLambda(event, context, callback);
    console.log('end init handler');
};