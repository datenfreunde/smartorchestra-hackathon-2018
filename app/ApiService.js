const rp = require('request-promise');
const LogUtil = require('./utils/LogUtil');
const settings = require('./settings');

class ApiService {

    constructor() {
    }

    getEndpoint() {
        let options = {
            uri: settings.BACKEND_URL + settings.ROUTES.endpoint,
            json: true
        };
        return rp(options).then((data) => {
            LogUtil.log('got endpoint data', data);
            if (Array.isArray(data)) {
                LogUtil.log('filter for: ' + settings.ENDPOINT_ID);
                let result = data.filter((d) => {
                    return d.ID === settings.ENDPOINT_ID;
                });
                return result[0].URL;
            } else {
                return data.URL;
            }

        }).catch((err) => {
            LogUtil.error('error getting items');
            return null;
        });
    }

    requestData(endpoint) {

        let options = {
            uri: endpoint,
            json: true,
            headers: {
                'Fiware-Service': 'fokuscustomer'
            }
        };

        return rp(options).then((data) => {
            LogUtil.log('got data from endpoint', data);
            return data;
        }).catch((err) => {
            LogUtil.error('error getting items');
            return null;
        });

    }

    getData() {
        return this.getEndpoint().then(
            (endpoint) => {
                LogUtil.log('using endpoint: ' + endpoint);
                if (endpoint !== null) {
                    return this.requestData(endpoint);
                }
                else {
                    return null;
                }
            },
        );
    }

    getDataDirect() {
        return this.requestData(settings.DIRECT_ENDPOINT);
    }

}

module.exports = ApiService;
