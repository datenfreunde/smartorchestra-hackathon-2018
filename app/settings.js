module.exports = {
    BACKEND_URL: 'https://bu2kvgg559.execute-api.eu-central-1.amazonaws.com/dev',
    ROUTES: {
        endpoint: '/'
    },
    ENDPOINT_ID: 'unistuttgart',
    DIRECT_ENDPOINT: 'http://smartorchestra.datenfreunde.net/e2watchreport/report.json',
    USE_DIRECT: true
};

