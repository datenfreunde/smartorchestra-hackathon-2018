class LogUtil {

    static log(...args) {
        console.log.apply(console, args);
    }

    static error(...args) {
        if( LogUtil._azureContext) {
            LogUtil._azureContext.error.apply( LogUtil._azureContext, args);
        }
        else {
            console.error.apply(console, args);
        }
    }
}

module.exports = LogUtil;
