#Demo Skill for the Smart Orchestra project

This code is part of the [SmartOrchestra](http://smartorchestra.de) project funded by the [Smart Service Initiative](https://ww
w.bmwi.de/DE/Themen/Digitale-Welt/Digitale-Technologien/smart-service-welt.html) of the Federal Ministery for Economic Affairs and
 Energy from 2016 to 2018. 


# What you need

- bespoken tools for local proxy 
  - `npm install -g bespoken-tools` 
  - [more infos](http://docs.bespoken.io/en/latest/)
- ask-cli for deployment
  - `npm install -g ask-cli`
  - [more infos](https://developer.amazon.com/de/docs/smapi/quick-start-alexa-skills-kit-command-line-interface.html#step-2-install-and-initialize-ask-cli)
  
# How to start

To be able to work with multiple people on this project every developer needs his own skill.
How to archive that?

- copy `config-dev` file to `config-YOUR-NAME`
- adjust all parameters inside:

parameter | info
--- | --- 
ASK_PROFILE=default | use the profile you created with ask init
SKILL_ID= | leave blank if you want to create a new skill id. After first deployment copy the skill-id here.
LAMBDA_URI=ask-custom-alexa-<YOUR-NAME>-dev-default | name for the lambda function
SKILL_NAME="SKILL NAME <YOUR-NAME>" | add your name shortcut to the name
USE_LOCAL_ENDPOINT=true | true or false
LOCAL_ENDPOINT_URI=YOUR BESPOKEN URL | url for local testing


## Local testing
- run `bst proxy lambda app/index.js`
- save the bespoken endpoint to your config_file -> LOCAL_ENDPOINT_URI
- run `./deploy.sh -c config_YOUR_NAME`

Now you have connected your local running "lambda" with the skill.

## Deployment & Update
Initial deployment and updating is done with the same command.
Attention: if you do not have defined a SKILL_ID in your config file `./deploy.sh` will create the skill for you.
After first deployment save this ID to your config. Otherwise you will generate a lot of skills in the Skill-Console.
- use your config-file for deployment: `./deploy.sh -c config_YOUR_NAME`

## get model from alexa skill console
- run `./getmodel.sh -c config_YOUR_NAME`


