#!/bin/bash
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -c|--config)
    config="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

if ! [ -f "$config" ]
then
    echo "config file ${config} does not exists"
    echo "usage: ./getmodel.sh -c config_dev"
    exit
else
    echo using config file = "${config}"
fi

source ${config}
ask api get-model -p $ASK_PROFILE -s $SKILL_ID -l de-DE > models/de-DE.json