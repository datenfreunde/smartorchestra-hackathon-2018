#!/bin/bash
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -c|--config)
    config="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

if ! [ -f "$config" ]
then
    echo "config file ${config} does not exists"
    echo "usage: ./deploy.sh -c config_dev"
    exit
else
    echo using config file = "${config}"
fi

# delete existing files
rm ./.ask/config
rm ./skill.json

source ${config}

if [ "$USE_LOCAL_ENDPOINT" == "true" ]
then
    localEndpoint="endpoint"
    lambdaEndpoint="__endpoint"
    merge="__merge"
else
    localEndpoint="__endpoint"
    lambdaEndpoint="endpoint"
    merge="merge"
fi

sed "s/__SKILL_ID__/${SKILL_ID}/g;\
     s/__LAMBDA_URI__/${LAMBDA_URI}/g;\
     s/__MERGE__/${merge}/g;\
     s/__ASK_PROFILE__/${ASK_PROFILE}/g;" \
     .ask/config-template > .ask/config


sed "s/__SKILL_NAME__/${SKILL_NAME}/g;\
     s/__USE_LOCAL_ENDPOINT__/${localEndpoint}/g;\
     s/__USE_LAMBDA_ENDPOINT__/${lambdaEndpoint}/g;\
     s,__LOCAL_ENDPOINT_URI__,${LOCAL_ENDPOINT_URI},g;" \
     skill-template.json > skill.json

# deploy
ask deploy -p $ASK_PROFILE